import datetime

def batt_rule(ts, sat_id, high_red, high_yellow, low_yellow, low_red, val, component):
    alert = None
    if val < low_red:
        # create key and timestamp value, if sat_id exists in dict, else update timestamp list for sat_id
        batt_dict.update({sat_id : [ts]}) if batt_dict.get(sat_id) == None else batt_dict.get(sat_id).append(ts)
        # measure ts_list for 3 elements
        if len(batt_dict.get(sat_id)) == 3:
            # if elements 0 and 2 are less than 5min apart, alert fires
            if batt_dict.get(sat_id)[2] - batt_dict.get(sat_id)[0] < 300.0:
                alert = {
                    'satellitesat_id' : sat_id,
                    'severity' : "RED LOW",
                    'component': "BATT",
                    'timestamp' : f"{datetime.datetime.fromtimestamp(batt_dict.get(sat_id)[0]).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]}Z",
                }
                # clear timestamps in ts_list for sat_id
                batt_dict.update({sat_id : None})
            # elements 0 and 2 are more than 5min apart, pop element 0 from sat_id ts_list
            else:
                batt_dict.get(sat_id).pop(0)
    return alert

def tstat_rule(ts, sat_id, high_red, high_yellow, low_yellow, low_red, val, component):
    alert = None
    if val > high_red:
        # create key and timestamp value, if sat_id exists in dict, else update timestamp list for sat_id
        tstat_dict.update({sat_id : [ts]}) if tstat_dict.get(sat_id) == None else tstat_dict.get(sat_id).append(ts)
        # measure ts_list for 3 elements
        if len(tstat_dict.get(sat_id)) == 3:
            # if elements 0 and 2 are less than 5min apart, alert fires
            if tstat_dict.get(sat_id)[2] - tstat_dict.get(sat_id)[0] < 300.0:
                alert = {
                    'satellitesat_id' : sat_id,
                    'severity' : "RED HIGH",
                    'component': "TSTAT",
                    'timestamp' : f"{datetime.datetime.fromtimestamp(tstat_dict.get(sat_id)[0]).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]}Z",
                }
                # clear timestamps in ts_list for sat_id
                tstat_dict.update({sat_id : None})
            # elements 0 and 2 are more than 5min apart, pop element 0 from sat_id ts_list
            else:
                tstat_dict.get(sat_id).pop(0)
    return alert

# create dict to switch between rules
def load_rules():
    return {
        'BATT' : batt_rule,
        'TSTAT' : tstat_rule,
    }

def main():
    filepath = 'data.txt'
    output = []
    # create global var to clear between runs
    global batt_dict
    global tstat_dict
    batt_dict = {}
    tstat_dict = {}

    with open(filepath, 'r') as f:
        # read one line at a time
        line = f.readline()
        while line:
            # split line on dilimeter '|' and map data elaments to vars
            split_line = line.split('|')
            ts = datetime.datetime.strptime(split_line[0], '%Y%m%d %H:%M:%S.%f').timestamp()
            sat_id = int(split_line[1])
            high_red = float(split_line[2])
            high_yellow = float(split_line[3])
            low_yellow = float(split_line[4])
            low_red = float(split_line[5])
            val = float(split_line[6])
            component = split_line[7].rstrip("\n")
            # capture result of rules selected based on component value
            result = load_rules()[component](ts, sat_id, high_red, high_yellow, low_yellow, low_red, val, component)
            # append valsat_id result to output list
            if result != None:
                output.append(result)
            # read next line
            line = f.readline()
        return output

if __name__ == '__main__':
    main()
